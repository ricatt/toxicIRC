toxicIRCAddon = {}
toxicIRCAddon.name = 'toxicIRC'
toxicIRCAddon.version = "0.9"

local LAM = LibStub("LibAddonMenu-2.0")
local username = string.sub(GetDisplayName(), 2)
local character = GetUnitName("player")

local panelData = {
     type = "panel",
     name = "ToxicIRC"
}

local optionsData = {
    [1] = {
            type = "colorpicker",
            name = "Text Color",
            getFunc = function() return toxicIRCAddon.savedVariables.colors.r, toxicIRCAddon.savedVariables.colors.g, toxicIRCAddon.savedVariables.colors.b end,
            setFunc = function(r,g,b,a) print(r,g,b) end,
            -- setFunc = function(r,g,b,a) toxicIRCAddon.savedVariables.colors.r=r, toxicIRCAddon.savedVariables.colors.g=g, toxicIRCAddon.savedVariables.colors.b=b end,
            tooltip = "Set the color of the text.",
            width = "full",
            requiresReload = false, -- boolean, if set to true, the warning text will contain a notice that changes are only applied after an UI reload and any change to the value will make the "Apply Settings" button appear on the panel which will reload the UI when pressed (optional)
            -- default = {r = defaults.r, g = defaults.g, b = defaults.b, a = defaults.a}, --(optional) table of default color values (or default = defaultColor, where defaultColor is a table with keys of r, g, b[, a]) or a function that returns the color
            -- reference = "MyAddonColorpicker" -- unique global reference to control (optional)
    }
}

function toxicIRCAddon:checkName(msg)
    local match = nil
    local patterns = {
        "("..username..")",
        "("..string.lower(username)..")",
        "("..character..")",
        "("..string.lower(character)..")"
    }

    for k, v in pairs(patterns) do
        if patterns[k] then
            match = string.match(msg, v)
            if type(match) == "string" then
                return match
            end
        end
    end
    return nil
end

function toxicIRCAddon:IRCStyle(text)
    local forward = ZO_ChatSystem_GetEventHandlers()[EVENT_CHAT_MESSAGE_CHANNEL];
    local rawEventUpdate = ZO_ChatSystem_AddEventHandler;

    rawEventUpdate(EVENT_CHAT_MESSAGE_CHANNEL, function(arg1, arg2, msg, ...)
        if not msg then
            return forward(arg1, arg2, msg, ...);
        end
        match = toxicIRCAddon:checkName(msg)
        if match then
            msg = string.gsub(msg, match, "|cffff00"..match.."|r")
        end
        return forward(arg1, arg2, msg, ...);
    end);

    ZO_ChatSystem_AddEventHandler = function(event, func)
        if event == EVENT_CHAT_MESSAGE_CHANNEL then
            forward = func;
        else
            rawEventUpdate(event, func);
        end
    end
end

function toxicIRCAddon:Initialize()
    EVENT_MANAGER:RegisterForEvent("toxicIRC", EVENT_PLAYER_ACTIVATED, function()
        EVENT_MANAGER:UnregisterForEvent("toxicIRC", EVENT_PLAYER_ACTIVATED)
        EVENT_MANAGER:RegisterForUpdate("toxicIRC", 1000, function()
            EVENT_MANAGER:UnregisterForUpdate"toxicIRC"
            toxicIRCAddon:IRCStyle()
        end)
    end)

    self.savedVariables = ZO_SavedVars:New("toxicIRCSavedVariables", 1, nil, {})
    if not self.savedVariables.colors then
        self.savedVariables.colors = {
            r = 1,
            g = 1,
            b = 0
        }
    end
    LAM:RegisterAddonPanel("MyAddonOptions", panelData)
    LAM:RegisterOptionControls("MyAddonOptions", optionsData)
end

function toxicIRCAddon.OnAddOnLoaded(event, addonName)
    if addonName == toxicIRCAddon.name then
        toxicIRCAddon:Initialize()
    end
end

EVENT_MANAGER:RegisterForEvent(toxicIRCAddon.name, EVENT_ADD_ON_LOADED, toxicIRCAddon.OnAddOnLoaded)